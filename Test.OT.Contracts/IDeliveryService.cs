﻿using System.ServiceModel;

namespace Test.OT.Contracts
{
    [ServiceContract]
    public interface IDeliveryService
    {
        [OperationContract]
        void DeliverMessage(EventMessage eventMessage);
    }
}