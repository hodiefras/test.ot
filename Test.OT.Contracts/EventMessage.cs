﻿using System.Runtime.Serialization;

namespace Test.OT.Contracts
{
    [DataContract]
    public class EventMessage
    {
        [DataMember]
        public string EventName { get; set; }

        [DataMember]
        public string Text { get; set; }

        [DataMember]
        public string ImportanceLevel { get; set; }
    }
}