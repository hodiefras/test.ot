﻿using Test.OT.Contracts;

namespace Test.OT.App
{
    /// <summary>
    ///     Factory for client communication
    ///     will be good in future for DI
    /// </summary>
    internal class CommunicationFactory
    {
        /// <summary>
        ///     Creation service fabric method
        /// </summary>
        /// <returns></returns>
        public static Communication<IDeliveryService> CreateDeliveryServiceCommunication()
        {
            var communication = new Communication<IDeliveryService>("Test.OT.Contracts.IDeliveryService_Endpoint");

            return communication;
        }
    }
}