﻿using NLog;
using Test.OT.Contracts;

namespace Test.OT.App
{
    internal class Program
    {
        private static readonly Logger Logger = LogManager.GetCurrentClassLogger();

        private static void Main(string[] args)
        {
            Logger.Debug("Create delivery service.");
            CommunicationFactory.CreateDeliveryServiceCommunication().Using(service =>
            {
                Logger.Debug("Create event message.");
                var eventMessage = new EventMessage
                {
                    EventName = "csharp",
                    ImportanceLevel = "Critical",
                    Text = "Some text"
                };
                service.DeliverMessage(eventMessage);
            });
        }
    }
}