﻿using System;
using System.ServiceModel;
using System.ServiceModel.Description;
using NLog;

namespace Test.OT.App
{
    public class Communication<T>
    {
        private static readonly Logger Logger = LogManager.GetCurrentClassLogger();

        private readonly string endpointName;

        private IEndpointBehavior endpointBehaviour;

        public Communication(string endpointName)
        {
            this.endpointName = endpointName;
        }

        public void RegisterBehaviour(IEndpointBehavior behaviour)
        {
            Logger.Debug("Trying to register behavior");
            endpointBehaviour = behaviour;
        }


        public void Using(Action<T> action)
        {
            // always new, not always correctly
            Logger.Debug("Creating fabric with previously specified endpoint");
            var channelFactory = new ChannelFactory<T>(endpointName);
            if (endpointBehaviour != null)
            {
                channelFactory.Endpoint.Behaviors.Add(endpointBehaviour);
            }
            // creating channel
            var channel = channelFactory.CreateChannel();
            try
            {
                action(channel);
                ((IClientChannel) channel).Close();
            }
            catch (Exception exception)
            {
                Logger.Error(exception, "Error with channel");
                var clientInstance = ((IClientChannel) channel);
                if (clientInstance.State == CommunicationState.Faulted)
                {
                    clientInstance.Abort();
                    channelFactory.Abort();
                }
                // close all
                else if (clientInstance.State != CommunicationState.Closed)
                {
                    clientInstance.Close();
                    channelFactory.Close();
                }
                throw;
            }
        }
    }
}