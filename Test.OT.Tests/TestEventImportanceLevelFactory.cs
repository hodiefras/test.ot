﻿using NUnit.Framework;
using Test.OT.DeliveryService.Data;

namespace Test.OT.Tests
{
    [TestFixture]
    public class TestEventImportanceLevelFactory
    {
        [Test]
        public void TestIncorrectData()
        {
            Assert.Catch(() => EventImportanceLevelFactory.CreateEventAndLevel("aaa", "import"));
        }
    }
}