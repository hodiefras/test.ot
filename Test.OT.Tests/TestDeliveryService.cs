﻿using NUnit.Framework;
using Test.OT.DeliveryService;

namespace Test.OT.Tests
{
    [TestFixture]
    public class TestDeliveryService
    {
        [Test]
        public void TestDeliverIncorrectMessage()
        {
            var service = new DeliveryService.DeliveryService(new Mediator());
            Assert.DoesNotThrow(() => service.DeliverMessage(null));
        }
    }
}