﻿using System;
using System.ServiceModel;
using Topshelf;

namespace Test.OT.DeliveryService
{
    public class WindowsService<T> : ServiceControl
    {
        private readonly Func<T> instanceCreator;
        private ServiceHost serviceHost;

        public WindowsService(Func<T> instanceCreator)
        {
            this.instanceCreator = instanceCreator;
        }

        public bool Start(HostControl hostControl)
        {
            serviceHost = new ServiceHost(instanceCreator());
            serviceHost.Open();

            return true;
        }

        public bool Stop(HostControl hostControl)
        {
            serviceHost.Close();

            return true;
        }
    }
}