﻿using Topshelf;

namespace Test.OT.DeliveryService
{
    internal class Program
    {
        private static void Main(string[] args)
        {
            var mediator = new Mediator();
            HostFactory.Run(x =>
            {
                x.Service(() => new WindowsService<DeliveryService>(() => new DeliveryService(mediator)));
                x.RunAsLocalSystem();
                x.SetDescription("DeliveryService");
                x.SetDisplayName("DeliveryService");
                x.SetServiceName("DeliveryService");
            });
        }
    }
}