﻿using NLog;

namespace Test.OT.DeliveryService.Data
{
    public class Channel : IChannel
    {
        private static readonly Logger Logger = LogManager.GetCurrentClassLogger();

        public virtual void Send(string text)
        {
            Logger.Info(text);
        }
    }
}