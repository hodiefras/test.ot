﻿using System;
using System.Collections.Generic;

namespace Test.OT.DeliveryService.Data
{
    public class ImportanceLevel
    {
        public ImportanceLevel(DAL.ImportanceLevel initLevel)
        {
            Name = initLevel.Name;
            Channels = new List<Channel>();
            foreach (var item in initLevel.Channels)
            {
                Channel channel = null;
                switch (item.Name.ToLowerInvariant())
                {
                    case "sms":
                        channel = new SmsChannel();
                        break;
                    case "e-mail":
                        channel = new EmailChannel();
                        break;
                }
                if (channel == null)
                    throw new ArgumentException("Incorrect channel name: {0}", initLevel.Name);

                Channels.Add(channel);
            }
        }

        public string Name { get; private set; }
        public IList<Channel> Channels { get; }
    }
}