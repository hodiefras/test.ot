﻿namespace Test.OT.DeliveryService
{
    public interface IChannel
    {
        void Send(string text);
    }
}