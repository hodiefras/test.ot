﻿using System;
using System.Linq;
using NLog;
using Test.OT.DAL;

namespace Test.OT.DeliveryService.Data
{
    /// <summary>
    ///     Fabric class, will remove when added DI-support
    /// </summary>
    public class EventImportanceLevelFactory
    {
        private static readonly Logger Logger = LogManager.GetCurrentClassLogger();


        /// <summary>
        ///     Must be same eventName and ImportanceLevel
        ///     TODO bad solution with tuple
        /// </summary>
        /// <param name="eventName"></param>
        /// <param name="importanceLevelName"></param>
        /// <returns></returns>
        public static Tuple<Event, ImportanceLevel> CreateEventAndLevel(string eventName, string importanceLevelName)
        {
            Event eventItem = null;
            ImportanceLevel importanceLevel = null;
            Logger.Info("Try to connect to Db");
            using (var deliveryContext = new DeliveryContext())
            {
                eventItem = new Event(deliveryContext.Events.First(t => t.Name.Equals(eventName)));
                importanceLevel =
                    new ImportanceLevel(deliveryContext.ImportanceLevels.First(t => t.Name.Equals(importanceLevelName)));
                Logger.Info("Items from db was sent. Wrap objects was created");
            }
            return Tuple.Create(eventItem, importanceLevel);
        }
    }
}