﻿using System.Collections.Generic;

namespace Test.OT.DeliveryService
{
    public class Event
    {
        public Event(DAL.Event initEvent)
        {
            Name = initEvent.Name;
            Subscribers = new List<string>();
            foreach (var item in initEvent.Subscribers)
            {
                Subscribers.Add(item.Name);
            }
        }

        public string Name { get; private set; }
        public IList<string> Subscribers { get; }
    }
}