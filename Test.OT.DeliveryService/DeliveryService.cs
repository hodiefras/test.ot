using System;
using System.ServiceModel;
using NLog;
using Test.OT.Contracts;
using Test.OT.DeliveryService.Data;

namespace Test.OT.DeliveryService
{
    [ServiceBehavior(InstanceContextMode = InstanceContextMode.Single)]
    public class DeliveryService : IDeliveryService
    {
        private static readonly Logger Logger = LogManager.GetCurrentClassLogger();

        private readonly Mediator mediator;

        public DeliveryService(Mediator initMediator)
        {
            mediator = initMediator;
        }

        public void DeliverMessage(EventMessage eventMessage)
        {
            if (mediator == null)
                return;
            try
            {
                if (eventMessage == null)
                    throw new ArgumentException();
                var messageItems = EventImportanceLevelFactory.CreateEventAndLevel(eventMessage.EventName,
                    eventMessage.ImportanceLevel);
                Logger.Info($"Was created {messageItems.Item1} and {messageItems.Item2}");
                mediator.SendMessage(messageItems, eventMessage.Text);
            }
            catch (Exception exception)
            {
                Logger.Error(() => $"{exception.Message} with {exception.StackTrace}");
            }
        }

        private void Test()
        {
            var eventMessage = new EventMessage
            {
                ImportanceLevel = "Critical",
                EventName = "all",
                Text = "bugaga"
            };

            var messageItems = EventImportanceLevelFactory.CreateEventAndLevel(eventMessage.EventName,
                eventMessage.ImportanceLevel);

            mediator.SendMessage(messageItems, eventMessage.Text);
        }
    }
}