using System;
using NLog;
using Test.OT.DeliveryService.Data;

namespace Test.OT.DeliveryService
{
    public class Mediator
    {
        private static readonly Logger Logger = LogManager.GetCurrentClassLogger();

        public void SendMessage(Tuple<Event, ImportanceLevel> messageItems, string text)
        {
            var eventItem = messageItems.Item1;
            var importanceLevel = messageItems.Item2;

            foreach (var subscriber in eventItem.Subscribers)
            {
                foreach (var channel in importanceLevel.Channels)
                {
                    Logger.Info(
                        $"Event {eventItem.Name} for {subscriber} will be send by channel {channel.GetType().Name} by level {importanceLevel.Name}");
                    channel.Send($"[{eventItem.Name}] {text}");
                }
            }
        }
    }
}