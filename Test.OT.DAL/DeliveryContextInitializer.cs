﻿using System.Collections.Generic;
using System.Data.Entity;

namespace Test.OT.DAL
{
    public class DeliveryContextInitializer : DropCreateDatabaseAlways<DeliveryContext>
    {
        protected override void Seed(DeliveryContext context)
        {
            IList<Event> eventList = new List<Event>();

            eventList.Add(new Event
            {
                Name = "php",
                Subscribers = new List<Subscriber>
                {
                    new Subscriber {Name = "Second"}
                }
            });
            eventList.Add(new Event
            {
                Name = "csharp",
                Subscribers = new List<Subscriber>
                {
                    new Subscriber {Name = "First"}
                }
            });
            eventList.Add(new Event
            {
                Name = "all",
                Subscribers = new List<Subscriber>
                {
                    new Subscriber {Name = "First"},
                    new Subscriber {Name = "Second"}
                }
            });

            IList<ImportanceLevel> importanceLevels = new List<ImportanceLevel>();

            importanceLevels.Add(new ImportanceLevel
            {
                Name = "Low",
                Channels = new List<Channel>
                {
                    new Channel {Name = "E-mail"}
                }
            });
            importanceLevels.Add(new ImportanceLevel
            {
                Name = "Middle",
                Channels = new List<Channel>
                {
                    new Channel {Name = "Sms"}
                }
            });
            importanceLevels.Add(new ImportanceLevel
            {
                Name = "Critical",
                Channels = new List<Channel>
                {
                    new Channel {Name = "Sms"},
                    new Channel {Name = "E-mail"}
                }
            });


            foreach (var item in eventList)
                context.Events.Add(item);
            foreach (var item in importanceLevels)
                context.ImportanceLevels.Add(item);

            base.Seed(context);
        }
    }
}