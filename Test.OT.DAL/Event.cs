﻿using System.Collections.Generic;

namespace Test.OT.DAL
{
    public class Event
    {
        public int EventId { get; set; }
        public string Name { get; set; }

        public virtual IList<Subscriber> Subscribers { get; set; }
    }
}