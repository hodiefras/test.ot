﻿namespace Test.OT.DAL
{
    public class Channel
    {
        public int ChannelId { get; set; }

        public string Name { get; set; }
    }
}