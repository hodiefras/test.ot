﻿using System.Collections.Generic;

namespace Test.OT.DAL
{
    public class ImportanceLevel
    {
        public int ImportanceLevelId { get; set; }
        public string Name { get; set; }
        public virtual IList<Channel> Channels { get; set; }
    }
}