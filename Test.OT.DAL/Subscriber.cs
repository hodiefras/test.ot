﻿namespace Test.OT.DAL
{
    public class Subscriber
    {
        public int SubscriberId { get; set; }
        public string Name { get; set; }
    }
}