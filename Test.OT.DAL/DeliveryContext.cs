﻿using System.Data.Entity;

namespace Test.OT.DAL
{
    public class DeliveryContext : DbContext
    {
        public DeliveryContext()
        {
            Database.SetInitializer(new DeliveryContextInitializer());
        }

        public DbSet<Event> Events { get; set; }
        public DbSet<ImportanceLevel> ImportanceLevels { get; set; }
    }
}